// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GurzhApps",
    defaultLocalization: "en",
    platforms: [.iOS(.v11)],
    products: [
        .library(
            name: "GurzhApps",
            targets: ["GurzhApps"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "GurzhApps",
            path: "Sources",
            resources: [.process("Resources")]
        ),
    ]
)
