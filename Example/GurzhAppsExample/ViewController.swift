import GurzhApps
import UIKit

final class ViewController: UIViewController {
    private let environment: GurzhApps.Environment = {
        let lang = Locale.current.languageCode ?? "en"
        return GurzhApps.Environment(
            app: "animal",
            lang: lang,
            basepath: "https://gurzhapps.ddns.net/",
            applicationName: "Example app",
            version: "Example version",
            device: "Example device",
            fullVersion: true
        )
    }()

    private let decoyPaymentLogin = "exampleapp@gurzh.com"

    private lazy var gurzhapps = GurzhApps(environment: environment, appearance: .default)

    private lazy var showFeedbackButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitleColor(.systemBlue, for: .normal)
        button.setTitle("Show feedback", for: .normal)
        button.addTarget(self, action: #selector(showFeedbackButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var altShowFeedbackButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitleColor(.systemGreen, for: .normal)
        button.setTitle("Show feedback alt colors", for: .normal)
        button.addTarget(self, action: #selector(altShowFeedbackButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var getDecoyPaymentTokenButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitleColor(.systemPink, for: .normal)
        button.setTitle("Get decoy payment", for: .normal)
        button.addTarget(self, action: #selector(getPaymentTokenButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var addDecoyPaymentTokenButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitleColor(.systemRed, for: .normal)
        button.setTitle("Add decoy payment", for: .normal)
        button.addTarget(self, action: #selector(addDecoyPaymentTokenButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var buttonsStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            showFeedbackButton,
            altShowFeedbackButton,
            getDecoyPaymentTokenButton,
            addDecoyPaymentTokenButton
        ])
        stack.axis = .vertical
        stack.spacing = 16
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        gurzhapps.decoyManager.getDecoyFeatures { result in
            print("DECOY FEATURES:")
            switch result {
            case .success(let features):
                print(features)
            case .failure(let error):
                print(error)
            }
        }

        gurzhapps.decoyManager.getDecoyPurchases { result in
            print("DECOY PURCHASES:")
            switch result {
            case .success(let purchases):
                print(purchases)
            case .failure(let error):
                print(error)
            }
        }
    }

    private func setupUI() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }

        view.addSubview(buttonsStackView)
        NSLayoutConstraint.activate([
            buttonsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            buttonsStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}

// MARK: - Feedback

extension ViewController {
    @objc func showFeedbackButtonTapped() {
        showFeedback(appearance: .default)
    }

    @objc func altShowFeedbackButtonTapped() {
        let appearance = GurzhApps.Appearance(
            backgroundColor: .black,
            fieldBackgroundColor: .systemGray,
            textColor: .white,
            placeholderColor: .init(white: 0.7, alpha: 1),
            tintColor: .systemGreen
        )
        showFeedback(appearance: appearance)
    }

    private func showFeedback(appearance: GurzhApps.Appearance) {
        gurzhapps = GurzhApps(environment: environment, appearance: appearance)
        gurzhapps.showFeedbackScreen(on: self)
    }
}

// MARK: - Decoy Payments

extension ViewController {
    @objc func getPaymentTokenButtonTapped() {
        gurzhapps.decoyManager.getDecoyPaymentToken(login: decoyPaymentLogin) { [weak self] result in
            switch result {
            case .success(let token):
                let text: String = {
                    if let token {
                        return "TOKEN=\(token)"
                    } else {
                        return "NO TOKEN"
                    }
                }()
                self?.showAlert(text: "\(text)")
            case .failure(let error):
                self?.showAlert(text: error.localizedDescription)
            }
        }
    }

    @objc func addDecoyPaymentTokenButtonTapped() {
        gurzhapps.decoyManager.sendDecoyPaymentToken(
            login: decoyPaymentLogin,
            token: "example_token",
            completion: { [weak self] _ in
                self?.showAlert(text: "COMPLETED")
            }
        )
    }

    private func showAlert(text: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
            alert.addAction(
                UIAlertAction(title: "OK", style: .cancel)
            )
            self.present(alert, animated: true)
        }
    }
}
