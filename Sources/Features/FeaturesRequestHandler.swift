public typealias GAFeatures = [String: Bool]

protocol FeaturesRequestHandler {
    func getDecoyFeatures(completion: @escaping (GAFeatures) -> Void)
}
