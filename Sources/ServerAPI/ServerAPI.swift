import Foundation

typealias PostRequestHandler = (GAError?) -> Void
typealias GetRequestHandler<T: Codable> = (Result<T, GAError>) -> Void

final class ServerAPI {
    private let basepath: String
    private let session: URLSession

    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()

    init(environment: GurzhApps.Environment) {
        self.basepath = environment.basepath

        let configuration: URLSessionConfiguration = .default
        configuration.httpAdditionalHeaders = [
            "app": environment.app,
            "lang": environment.lang
        ]
        session = URLSession(configuration: configuration)
    }

    func postRequest(
        path: String,
        data: Data,
        completion: @escaping PostRequestHandler
    ) {
        let urlPath = basepath + path
        guard let url = URL(string: urlPath) else {
            completion(.incorrectUrl(path: urlPath))
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = data

        session.dataTask(with: request) { data, response, error in
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? .zero
            printdbg("[API POST] RESPONSE \(String(statusCode)) \(url)")
            guard let data = data else {
                completion(.error(error))
                return
            }
            printdbgJSON(data)
            completion(nil)
        }.resume()
    }

    func getRequest<T: Codable>(
        path: String,
        queryItems: [String: String] = [:],
        completion: @escaping GetRequestHandler<T>
    ) {
        let urlPath = basepath + path
        var urlComponents = URLComponents(string: urlPath)
        if queryItems.isEmpty == false {
            urlComponents?.queryItems = queryItems.map {
                URLQueryItem(name: $0.key, value: $0.value)
            }
        }

        guard let url = urlComponents?.url else {
            completion(.failure(.incorrectUrl(path: urlPath)))
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        session.dataTask(with: request) { [weak self] data, response, error in
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? .zero
            printdbg("[API GET] RESPONSE \(String(statusCode)) \(url)")
            guard let data = data, let result: T = self?.decode(data) else {
                completion(.failure(.error(error)))
                return
            }
            printdbgJSON(data)
            completion(.success(result))
        }.resume()
    }

    func decode<T>(_ data: Data) -> T? where T: Codable {
        do {
            let decoded = try decoder.decode(T.self, from: data)
            return decoded
        } catch {
            printdbg(error.localizedDescription)
            return nil
        }
    }

    func encode<T>(_ value: T) -> Data? where T: Encodable {
        do {
            let jsonData = try encoder.encode(value)
            return jsonData
        } catch {
            printdbg(error.localizedDescription)
            return nil
        }
    }
}
