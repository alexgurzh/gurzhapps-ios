extension ServerAPI: FeaturesRequestHandler {
    func getDecoyFeatures(completion: @escaping (GAFeatures) -> Void) {
        getRequest(
            path: "features.php",
            completion: { (result: Result<GAFeatures, GAError>) in
                switch result {
                case .success(let response):
                    completion(response)
                case .failure(let error):
                    printdbg(error.localizedDescription)
                    completion(emptyFeatures)
                }
            }
        )
    }
}

fileprivate let emptyFeatures: GAFeatures = [:]
