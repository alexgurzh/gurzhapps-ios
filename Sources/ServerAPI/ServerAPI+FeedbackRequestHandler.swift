extension ServerAPI: FeedbackRequestHandler {
    func sendFeedback(model: FeedbackRequestModel, completion: @escaping (Bool) -> Void) {
        guard let jsonData = encode(model) else {
            completion(false)
            return
        }

        postRequest(path: "feedback.php", data: jsonData) { error in
            if let error = error {
                printdbg(error.localizedDescription)
                completion(false)
                return
            }
            completion(true)
        }
    }
}
