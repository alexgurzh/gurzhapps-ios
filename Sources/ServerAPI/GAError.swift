import Foundation

enum GAError: Error, LocalizedError {
    case incorrectUrl(path: String)
    case error(_ error: Error?)
    case unknown

    var localizedDescription: String {
        switch self {
        case .incorrectUrl(let path):
            return "Incorrect URL \(path)"
        case .error(let error):
            return "Error: \(error?.localizedDescription ?? "<no data>")"
        case .unknown:
            return "Unknown error"
        }
    }
}
