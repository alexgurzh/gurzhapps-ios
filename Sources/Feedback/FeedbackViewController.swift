import UIKit

protocol FeedbackViewControllerDelegate: AnyObject {
    func cancelButtonTapped()
    func sendButtonTapped(model: FeedbackModel)
}

final class FeedbackViewController: UIViewController {
    private enum Constants {
        static let offset: CGFloat = 16
        static let smallOffset: CGFloat = 8
        static let bigOffset: CGFloat = 32
        static let textViewMinHeight: CGFloat = 120
        static let textFieldInsets: UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        static let font: UIFont = UIFont.systemFont(ofSize: 16)
    }

    weak var delegate: FeedbackViewControllerDelegate?

    private let appearance: GurzhApps.Appearance

    private lazy var nameField = makeNameField()
    private lazy var emailField = makeEmailField()
    private lazy var anonymousSwitch = UISwitch()
    private lazy var anonymousCheckboxView = makeAnonymousCheckbox()
    private lazy var textView = makeTextView()

    private var textViewHeightConstaint: NSLayoutConstraint?

    private var feedbackModel = FeedbackModel()

    init(appearance: GurzhApps.Appearance) {
        self.appearance = appearance
        super.init(nibName: nil, bundle: nil)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateState()
    }

    private func updateState() {
        let isContactsRequired = feedbackModel.isAnonymous == false
        nameField.isEnabled = isContactsRequired
        emailField.isEnabled = isContactsRequired
        let isContactsFilled = nameField.text.isEmpty == false && emailField.text.isEmpty == false
        navigationItem.rightBarButtonItem?.isEnabled =
            (isContactsFilled || isContactsRequired == false)
            && feedbackModel.text.isEmpty == false
    }

    private func updateTextViewHeightIfNeeded() {
        let textHeight = max(textView.textHeight, Constants.textViewMinHeight)
        guard let textViewHeightConstaint = textViewHeightConstaint,
              textViewHeightConstaint.constant != textHeight else {
            return
        }

        textViewHeightConstaint.constant = textHeight
        view.layoutIfNeeded()
    }

    @objc private func anonymousSwitchValueChanged(sender: UISwitch) {
        feedbackModel.isAnonymous = anonymousSwitch.isOn
        updateState()
    }

    @objc private func cancelButtonTapped(sender: UIBarButtonItem) {
        delegate?.cancelButtonTapped()
    }

    @objc private func sendButtonTapped(sender: UISwitch) {
        delegate?.sendButtonTapped(model: feedbackModel)
    }

    @objc private func nameFieldDidChange(sender: UITextField) {
        feedbackModel.name = nameField.text
        updateState()
    }

    @objc private func emailFieldDidChange(sender: UITextField) {
        feedbackModel.contact = emailField.text
        updateState()
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate
extension FeedbackViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        feedbackModel.text = textView.text
        updateState()
        updateTextViewHeightIfNeeded()
    }
}

// MARK: - Setup UI
extension FeedbackViewController {
    private func setupUI() {
        view.backgroundColor = appearance.backgroundColor
        view.tintColor = appearance.tintColor
        setupNavigationItem()
        setupTexts()
        setupViews()
        setupGestures()
    }

    private func setupNavigationItem() {
        let titleTextAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: appearance.textColor]
        UINavigationBar.appearance().titleTextAttributes = titleTextAttributes
        if #available(iOS 13.0, *) {
            let barAppearance = UINavigationBarAppearance()
            barAppearance.configureWithOpaqueBackground()
            barAppearance.backgroundColor = appearance.backgroundColor
            barAppearance.titleTextAttributes = titleTextAttributes
            barAppearance.largeTitleTextAttributes = titleTextAttributes
            navigationController?.navigationBar.standardAppearance = barAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = barAppearance
        } else {
            UINavigationBar.appearance().barTintColor = appearance.backgroundColor
            UINavigationBar.appearance().isTranslucent = false
        }

        navigationItem.title = "feedback.title".localized

        let barButtonColor = appearance.tintColor
        let barButtonDisabledTextAttributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: barButtonColor.withAlphaComponent(0.3)
        ]

        let leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(cancelButtonTapped)
        )
        leftBarButtonItem.tintColor = barButtonColor
        leftBarButtonItem.setTitleTextAttributes(barButtonDisabledTextAttributes, for: .disabled)
        navigationItem.leftBarButtonItem = leftBarButtonItem

        let rightBarButtonItem = UIBarButtonItem(
            title: "feedback.button.send".localized,
            style: .done,
            target: self,
            action: #selector(sendButtonTapped)
        )
        rightBarButtonItem.tintColor = barButtonColor
        rightBarButtonItem.setTitleTextAttributes(barButtonDisabledTextAttributes, for: .disabled)
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }

    private func setupTexts() {
        nameField.attributedPlaceholder = NSAttributedString(
            string: "feedback.name.placeholder".localized,
            attributes: [.foregroundColor: appearance.placeholderColor]
        )
        emailField.attributedPlaceholder = NSAttributedString(
            string: "feedback.email.placeholder".localized,
            attributes: [.foregroundColor: appearance.placeholderColor]
        )
        textView.placeholder = "feedback.text.placeholder".localized
        textView.placeholderColor = appearance.placeholderColor
    }

    private func setupViews() {
        addView(nameField, offset: Constants.smallOffset)
        addView(emailField, under: nameField, offset: Constants.smallOffset)
        addView(anonymousCheckboxView, under: emailField, offset: Constants.offset)
        addView(textView, under: anonymousCheckboxView, offset: Constants.offset)

        textViewHeightConstaint = textView.heightAnchor.constraint(
            greaterThanOrEqualToConstant: Constants.textViewMinHeight
        )
        textViewHeightConstaint?.isActive = true
    }

    private func addView(_ viewToAdd: UIView, under upperView: UIView? = nil, offset: CGFloat = 0) {
        view.addSubview(viewToAdd)
        viewToAdd.translatesAutoresizingMaskIntoConstraints = false

        let topConstraint: NSLayoutConstraint = {
            if let upperView = upperView {
                return viewToAdd.topAnchor.constraint(
                    equalTo: upperView.bottomAnchor,
                    constant: offset
                )
            }
            else {
                return viewToAdd.topAnchor.constraint(
                    equalTo: view.safeAreaLayoutGuide.topAnchor,
                    constant: offset
                )
            }
        }()
        NSLayoutConstraint.activate([
            topConstraint,
            viewToAdd.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Constants.offset),
            viewToAdd.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -Constants.offset)
        ])
    }

    private func setupGestures() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }

    private func makeNameField() -> UITextField {
        let textField = GATextField(frame: .zero)
        textField.backgroundColor = appearance.fieldBackgroundColor
        textField.textColor = appearance.textColor
        textField.textInsets = Constants.textFieldInsets
        textField.font = Constants.font
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(nameFieldDidChange), for: .editingChanged)
        return textField
    }

    private func makeEmailField() -> UITextField {
        let textField = GATextField(frame: .zero)
        textField.backgroundColor = appearance.fieldBackgroundColor
        textField.textColor = appearance.textColor
        textField.textInsets = Constants.textFieldInsets
        textField.font = Constants.font
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(emailFieldDidChange), for: .editingChanged)
        return textField
    }

    private func makeAnonymousCheckbox() -> UIView {
        let containter = UIView()
        let label = UILabel()
        label.text = "feedback.anonymous.title".localized
        label.font = Constants.font
        label.textColor = appearance.textColor

        anonymousSwitch.tintColor = appearance.tintColor
        anonymousSwitch.onTintColor = appearance.tintColor
        anonymousSwitch.addTarget(self, action: #selector(anonymousSwitchValueChanged), for: .valueChanged)

        containter.addSubview(label)
        containter.addSubview(anonymousSwitch)

        containter.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        anonymousSwitch.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: containter.topAnchor),
            label.bottomAnchor.constraint(equalTo: containter.bottomAnchor),
            label.leftAnchor.constraint(equalTo: containter.leftAnchor),
            anonymousSwitch.topAnchor.constraint(equalTo: containter.topAnchor),
            anonymousSwitch.bottomAnchor.constraint(equalTo: containter.bottomAnchor),
            anonymousSwitch.leftAnchor.constraint(equalTo: label.rightAnchor, constant: Constants.offset),
            anonymousSwitch.rightAnchor.constraint(equalTo: containter.rightAnchor)
        ])

        return containter
    }

    private func makeTextView() -> GATextView {
        let textView = GATextView()
        textView.backgroundColor = appearance.fieldBackgroundColor
        textView.textColor = appearance.textColor
        textView.font = Constants.font
        textView.textContainerInset = Constants.textFieldInsets
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.delegate = self
        return textView
    }
}
