struct FeedbackRequestModel: Codable {
    let app: String
    let version: String
    let device: String
    let fullVersion: Bool
    let name: String?
    let contact: String?
    let text: String

    init?(environment: GurzhApps.Environment, model: FeedbackModel) {
        guard let text = model.text else {
            return nil
        }

        self.app = environment.applicationName
        self.version = environment.version
        self.device = environment.device
        self.fullVersion = environment.fullVersion
        self.name = model.name
        self.contact = model.contact
        self.text = text
    }
}

protocol FeedbackRequestHandler {
    func sendFeedback(model: FeedbackRequestModel, completion: @escaping (Bool) -> Void)
}
