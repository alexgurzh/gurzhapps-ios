struct FeedbackModel {
    var name: String?
    var contact: String?
    var isAnonymous: Bool = false
    var text: String?
}
