import UIKit

protocol FeedbackService {
    func showFeedbackScreen(on parent: UIViewController, appearance: GurzhApps.Appearance)
}

final class FeedbackServiceImpl {
    private let environment: GurzhApps.Environment
    private let requestHandler: FeedbackRequestHandler

    private weak var navigationController: UINavigationController?

    init(environment: GurzhApps.Environment, requestHandler: FeedbackRequestHandler) {
        self.environment = environment
        self.requestHandler = requestHandler
    }
}

// MARK: - FeedbackService
extension FeedbackServiceImpl: FeedbackService {
    func showFeedbackScreen(on parent: UIViewController, appearance: GurzhApps.Appearance) {
        let feedbackVC = FeedbackViewController(appearance: appearance)
        feedbackVC.delegate = self
        let navigationController = UINavigationController(rootViewController: feedbackVC)
        parent.present(navigationController, animated: true, completion: nil)
        self.navigationController = navigationController
    }
}

// MARK: - FeedbackViewControllerDelegate
extension FeedbackServiceImpl: FeedbackViewControllerDelegate {
    func cancelButtonTapped() {
        close()
    }

    func sendButtonTapped(model: FeedbackModel) {
        guard
            let requestModel = FeedbackRequestModel(
                environment: environment,
                model: model
            )
        else {
            showFeedbackError()
            return
        }

        requestHandler.sendFeedback(model: requestModel) { [weak self] success in
            DispatchQueue.main.async {
                guard success else {
                    self?.showFeedbackError()
                    return
                }
                self?.close()
            }
        }
    }

    private func close() {
        navigationController?.dismiss(animated: true, completion: nil)
    }

    private func showFeedbackError() {
        // TODO: Обработка ошибки
    }
}

