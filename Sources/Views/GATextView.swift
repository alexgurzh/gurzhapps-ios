import UIKit

public final class GATextView: UITextView {
    public var placeholder: String? {
        didSet {
            placeholderLabel.text = placeholder
            updatePlaceholderVisibility()
            layoutIfNeeded()
        }
    }

    public var placeholderColor: UIColor = .lightGray {
        didSet {
            placeholderLabel.textColor = placeholderColor
        }
    }

    private lazy var placeholderLabel = makePlaceholderLanbel()

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        updatePlaceholderFrame()
    }

    public var textHeight: CGFloat {
        sizeThatFits(
            CGSize(width: frame.size.width, height: CGFloat.greatestFiniteMagnitude)
        ).height
    }

    private func updatePlaceholderFrame() {
        let placeholderSize = placeholderLabel.sizeThatFits(contentSize)
        let placeholderFrame = CGRect(
            x: textContainerInset.right,
            y: textContainerInset.top,
            width: placeholderSize.width,
            height: placeholderSize.height
        )
        placeholderLabel.frame = placeholderFrame
    }

    private func updatePlaceholderVisibility() {
        placeholderLabel.isHidden = !text.isEmpty
    }
}

// MARK: - Setup UI
extension GATextView {
    private func setupUI() {
        textStorage.delegate = self
        textContainer.lineFragmentPadding = 0
        addSubview(placeholderLabel)
    }

    private func makePlaceholderLanbel() -> UILabel {
        let placeholderLabel = UILabel()
        placeholderLabel.text = placeholder
        placeholderLabel.numberOfLines = 0
        placeholderLabel.lineBreakMode = .byWordWrapping
        placeholderLabel.font = font
        placeholderLabel.textColor = placeholderColor
        return placeholderLabel
    }
}

// MARK: - UITextViewDelegate
extension GATextView: NSTextStorageDelegate {
    public func textStorage(
        _ textStorage: NSTextStorage,
        didProcessEditing editedMask: NSTextStorage.EditActions,
        range editedRange: NSRange,
        changeInLength delta: Int
    ) {
        updatePlaceholderVisibility()
    }
}
