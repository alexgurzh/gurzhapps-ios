import UIKit

public final class GurzhApps {
    public let inAppManager: InAppManager

    private let environment: Environment
    private let appearance: Appearance

    private let serverApi: ServerAPI
    private let feedbackService: FeedbackService

    public init(
        environment: Environment,
        appearance: Appearance,
        inApps: [InAppProductIdentifier] = []
    ) {
        self.environment = environment
        self.appearance = appearance
        self.serverApi = ServerAPI(environment: environment)
        self.feedbackService = FeedbackServiceImpl(environment: environment, requestHandler: serverApi)
        self.inAppManager = InAppManagerImpl(productsIdentifiers: inApps)
    }

    // MARK: Feedback

    public func showFeedbackScreen(on parent: UIViewController) {
        feedbackService.showFeedbackScreen(on: parent, appearance: appearance)
    }

    // MARK: Features

    public func getFeatures(completion: @escaping (GAFeatures) -> Void) {
        serverApi.getDecoyFeatures(completion: completion)
    }
}
