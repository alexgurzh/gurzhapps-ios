import UIKit

extension GurzhApps {
    public struct Appearance {
        let backgroundColor: UIColor
        let fieldBackgroundColor: UIColor
        let textColor: UIColor
        let placeholderColor: UIColor
        let tintColor: UIColor

        public init(
            backgroundColor: UIColor,
            fieldBackgroundColor: UIColor,
            textColor: UIColor,
            placeholderColor: UIColor,
            tintColor: UIColor
        ) {
            self.backgroundColor = backgroundColor
            self.fieldBackgroundColor = fieldBackgroundColor
            self.textColor = textColor
            self.placeholderColor = placeholderColor
            self.tintColor = tintColor
        }

        @available(iOS 13.0, *)
        public static let `default` = GurzhApps.Appearance(
            backgroundColor: .systemBackground,
            fieldBackgroundColor: .secondarySystemBackground,
            textColor: .label,
            placeholderColor: .placeholderText,
            tintColor: .systemBlue
        )
    }
}
