import Foundation

extension Float {
    func formattedPriceText(locale: Locale = Locale.current) -> String {
        let priceFormatter: NumberFormatter = NumberFormatter()
        priceFormatter.formatterBehavior = NumberFormatter.Behavior.behavior10_4
        priceFormatter.numberStyle = NumberFormatter.Style.currency
        priceFormatter.locale = locale
        return priceFormatter.string(from: NSNumber(value: self)) ?? "***"
    }
}
