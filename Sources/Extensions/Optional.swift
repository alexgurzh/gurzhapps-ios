extension Optional where Wrapped: Collection {
    var isEmpty: Bool {
        return self?.isEmpty ?? true
    }
}
