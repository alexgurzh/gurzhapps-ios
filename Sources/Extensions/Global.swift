import Foundation

func printdbg(_ string: String) {
    #if DEBUG
    print("GurzhApps: \(string)")
    #endif
}

func printdbgJSON(_ data: Data) {
    #if DEBUG
    if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) {
        print("GurzhApps JSON: \(jsonData)")
    } else {
        print("GurzhApps Data: \(data)")
    }
    #endif
}
