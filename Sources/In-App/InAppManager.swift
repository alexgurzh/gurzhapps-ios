import UIKit
import StoreKit

public typealias InAppProductIdentifier = String
public typealias InAppPurchaseCompletion = (Bool) -> Void
public typealias InAppRequestCompletion = (_ products: [InAppProduct]) -> ()

public struct InAppProduct {
    public let identifier: InAppProductIdentifier
    public let localizedTitle: String
    public let priceText: String
}

public protocol InAppManager {
    func buyProduct(_ productIdentifier: InAppProductIdentifier, completion: InAppPurchaseCompletion?)
    func restoreCompletedTransactions(_ completion: InAppPurchaseCompletion?)
    func requestProducts(_ completionHandler: @escaping InAppRequestCompletion)
    func askForProductWithAlert(
        _ identifier: InAppProductIdentifier,
        on viewController: UIViewController,
        alertData: (title: String, subtitle: String)?,
        completion: @escaping InAppPurchaseCompletion
    )
}

// MARK: Internal Implementation

final class InAppManagerImpl: NSObject {
    private let productsIdentifiers: [InAppProductIdentifier]

    private let userDefaults = UserDefaults.standard
    private let queue = SKPaymentQueue.default()

    private var productsRequest: SKProductsRequest?
    private var requestCompletion: InAppRequestCompletion?
    private var purchaseCompletion: InAppPurchaseCompletion?

    private var cachedProducts: [SKProduct] = []

    init(productsIdentifiers: [InAppProductIdentifier]) {
        self.productsIdentifiers = productsIdentifiers
        super.init()
        queue.add(self)
    }

    private func showTrialAlert(
        for product: InAppProduct,
        on viewController: UIViewController,
        alertData: (title: String, subtitle: String)?,
        completion: @escaping InAppPurchaseCompletion
    ) {
        guard let alertData = alertData else {
            buyProduct(product.identifier, completion: completion)
            return
        }

        let alert = UIAlertController(
            title: alertData.title,
            message: alertData.subtitle,
            preferredStyle: .alert
        )

        let buyString = "\("inapp.alert.buy".localized) (\(product.priceText))"
        let alertBuy = UIAlertAction(title: buyString, style: .default, handler: { [weak self] _ in
            self?.buyProduct(product.identifier, completion: completion)
        })
        alert.addAction(alertBuy)

        let alertCancel = UIAlertAction(title: "inapp.alert.cancel".localized, style: .cancel, handler: { _ in
            completion(false)
        })
        alert.addAction(alertCancel)

        viewController.present(alert, animated: true, completion: nil)
    }
}

// MARK: - InAppManager
extension InAppManagerImpl: InAppManager {
    func requestProducts(_ completionHandler: @escaping InAppRequestCompletion) {
        guard productsIdentifiers.isEmpty == false else {
            completionHandler([])
            return
        }

        requestCompletion = completionHandler
        productsRequest = SKProductsRequest(productIdentifiers: Set(productsIdentifiers))
        productsRequest?.delegate = self
        productsRequest?.start()
    }

    func buyProduct(_ productIdentifier: InAppProductIdentifier, completion: InAppPurchaseCompletion?) {
        purchaseCompletion = completion

        let buyAction: () -> Bool = { [weak self] in
            guard let product = self?.cachedProducts.first(where: { $0.productIdentifier == productIdentifier }) else {
                return false
            }

            self?.queue.add(SKPayment(product: product))
            return true
        }

        guard buyAction() != false else {
            requestProducts { [weak self] _ in
                guard buyAction() else {
                    self?.purchaseCompletion?(false)
                    self?.purchaseCompletion = nil
                    printdbg("InAppManager: Can't get product \(productIdentifier)")
                    return
                }
            }
            return
        }
    }

    func restoreCompletedTransactions(_ completion: InAppPurchaseCompletion? = nil) {
        purchaseCompletion = completion
        queue.restoreCompletedTransactions()
    }

    func askForProductWithAlert(
        _ identifier: InAppProductIdentifier,
        on viewController: UIViewController,
        alertData: (title: String, subtitle: String)?,
        completion: @escaping InAppPurchaseCompletion
    ) {
        requestProducts { [weak self] products in
            guard let product = products.first(where: { $0.identifier == identifier }) else {
                completion(false)
                return
            }

            DispatchQueue.main.async {
                self?.showTrialAlert(
                    for: product,
                    on: viewController,
                    alertData: alertData,
                    completion: completion
                )
            }
        }
    }
}

// MARK: - SKProductsRequestDelegate
extension InAppManagerImpl: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        productsRequest = nil

        cachedProducts = response.products
        let inAppProducts: [InAppProduct] = response.products.compactMap { product in
            let priceText = product.price.floatValue.formattedPriceText(locale: product.priceLocale)
            printdbg("InAppManager: found product \(product.productIdentifier) \(product.localizedTitle) \(priceText)")
            return InAppProduct(
                identifier: product.productIdentifier,
                localizedTitle: product.localizedTitle,
                priceText: priceText
            )
        }

        DispatchQueue.main.async { [weak self] in
            self?.requestCompletion?(inAppProducts)
            self?.requestCompletion = nil
        }
    }

    func request(_ request: SKRequest, didFailWithError error: Error) {
        productsRequest = nil

        DispatchQueue.main.async { [weak self] in
            self?.requestCompletion?([])
            self?.requestCompletion = nil
        }
    }
}

// MARK: - SKPaymentTransactionObserver
extension InAppManagerImpl: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                completeTransaction(transaction)
            case .failed:
                failedTransaction(transaction)
            case .restored:
                restoreTransaction(transaction)
            case .purchasing:
                return
            default:
                failedTransaction(transaction)
            }
        }
    }

    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        printdbg("InAppManager restoreCompletedTransactionsFailedWithError: \(error.localizedDescription)")

        DispatchQueue.main.async { [weak self] in
            self?.purchaseCompletion?(false)
            self?.purchaseCompletion = nil
        }
    }

    private func completeTransaction(_ transaction: SKPaymentTransaction) {
        let productIdentifier = transaction.payment.productIdentifier
        guard productsIdentifiers.contains(productIdentifier) else {
            failedTransaction(transaction)
            return
        }

        provideContent(for: productIdentifier)
        queue.finishTransaction(transaction)
    }

    private func restoreTransaction(_ transaction: SKPaymentTransaction) {
        guard
            let productIdentifier = transaction.original?.payment.productIdentifier,
            productsIdentifiers.contains(productIdentifier)
        else {
            failedTransaction(transaction)
            return
        }

        provideContent(for: productIdentifier)
        queue.finishTransaction(transaction)
    }

    private func failedTransaction(_ transaction: SKPaymentTransaction) {
        if let errorDescription = transaction.error?.localizedDescription {
            printdbg("InAppManager transaction error: \(errorDescription)")
        }

        DispatchQueue.main.async { [weak self] in
            self?.purchaseCompletion?(false)
            self?.purchaseCompletion = nil
        }

        queue.finishTransaction(transaction)
    }

    private func provideContent(for productIdentifier: InAppProductIdentifier) {
        userDefaults.set(true, forKey: productIdentifier)
        userDefaults.synchronize()

        DispatchQueue.main.async { [weak self] in
            self?.purchaseCompletion?(true)
            self?.purchaseCompletion = nil
        }
    }
}
