import Foundation

extension GurzhApps {
    public struct Environment {
        let app: String
        let lang: String
        let basepath: String
        let applicationName: String
        let version: String
        let device: String
        let fullVersion: Bool

        public init(
            app: String,
            lang: String,
            basepath: String,
            applicationName: String,
            version: String,
            device: String,
            fullVersion: Bool
        ) {
            self.app = app
            self.lang = lang
            self.basepath = basepath
            self.applicationName = applicationName
            self.version = version
            self.device = device
            self.fullVersion = fullVersion
        }
    }
}
